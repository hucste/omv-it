#!/bin/sh
#set -x

###
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
# gpg:fingerprint: CE2C CF7C AB68 0329 0D20  5F49 6135 D440 4D44 BD58
#
# License: BSD Simplified
#
# Github: git@framagit.org:hucste/omv-it.git
#
# Date: 2018/10/13
#
###

################################################################################
###
##
#  INFOS: NOT USE; Just for test!
##
###
################################################################################

dir_mnt="/mnt/nas"  
dir_root="/root/Administration"
dir_tmp="/tmp"

################################################################################
###
##
#   VARIABLES; DO NOT TOUCH - LEAVE EMPTY
##
###
################################################################################

error=0 
device=""  
dir_duid="" # mount point of crypted hd
hd=""    # diskname
file_passwd="" # passwd file of crypted hd
partition=""    
s=""    # letter of slide
slide=""
symlink=""

################################################################################

#set -A PARAMS -- "$@"
#menu="$(echo "${PARAMS[0]}" | tr "[:upper:]" "[:lower:]")"
#option="$(echo "${PARAMS[1]}" | tr "[:upper:]" "[:lower:]")"
menu="$1"
option="$2"

################################################################################
###
##
#   FUNCTIONS
##
###
################################################################################
attach_disk() {
    # attempt to open HD
    logger "$0: bioctl -c C -l ${slide} -p ${file_passwd} "
    if bioctl -c C -l "${slide}" -p "${file_passwd}" softraid0 > "${file_tmp}"; then
        echo "*** bioctl attaches from ${slide}: OK!"
        chmod 0400 "${file_tmp}"
    
    else
        echo "*** bioctl attaches from ${slide}: KO!"
        
    fi
    
}

build_dirs() {
    
    get_symlink
    
    dir_duid="/mnt/${duid}"
    if [ ! -d "${dir_duid}" ] ; then 
        mkdir -p "${dir_duid}"
        set_symlink
    fi
    
}

check_fs() {
    
    echo "*** Mount ${hd}: KO!"
    echo "*** Init a FS check on ${partition}!"
    logger "$0: Can't mount ${hd}; attempt FS check on ${partition}!"
                    
    if fsck -y "/dev/${partition}"; then
        echo "*** Fsck seems: OK!"
        logger "$0: FS check on ${partition}: OK!"
        
        return 0;
          
    else
        echo "*** Fsck seems: KO!"
        logger "$0: FS check on ${partition}: KO! Can't mount ${hd}!"
        
        return 1;
          
    fi
    
}

delete_all() {
    
    # if /mnt/$duid is empty, then delete it and his symlink
    if [ -z "$(find "${dir_duid}/". -name . -o -print | head -n 1)" ]; then 
        rm -fPR "${dir_duid}"
        rm "${symlink}"
    fi
    sleep 1
    
}

get_device() {
    device="$(awk '/CRYPTO/ { printf "%s",$6 }' "${file_tmp}")"
}

get_disk() {
    
    case "$option" in
        "bckp"|"sd1") hd="sd1" ;;
        "nas"|"sd0")   hd="sd0" ;;
    esac
    
}

get_duid() {
    # get duid info
    duid="$(disklabel "${hd}" | awk -F':' '/duid/ { gsub(/[[:space:]]*/, "", $2) ; printf "%s",$2 }')"
}

get_passwd() {
    file_passwd="${dir_root}/.${duid}"
    #echo "file passwd: $file_passwd"
}

get_slide() {
    #disk="${hd}"
    case "${hd}" in
        "sd0"|"sd1") pattern="RAID" ;;
        "sd5") pattern="4.2BSD" ;;
    esac
    
    # get partition's letter
    s="$(disklabel "${hd}" | awk -F':' '/'${pattern}'/ { gsub(/[[:space:]]*/, "", $1); printf "%s",$1 }' )"
    
    partition="${hd}$s"
    slide="${duid}.$s"

    unset pattern
}

get_symlink() {
    symlink="${dir_mnt}"
}

get_tmp() {
    file_tmp="${dir_tmp}/${duid}.info"
}

help() {
	
	echo "
$0 mount raid   # to mount crypted hd for nas or backup
$0 umount raid  # to unmount crypted hd to nas or backup
"
	
}

_mount() {
    logger "$0: mount ${partition} to ${dir_duid}!"
    if mount -o noatime,nodev,noexec,nosuid,rw,softdep "/dev/${partition}" "${dir_duid}"; then
        echo "*** Mount ${hd}: OK!"
        return 0
        
    else
        echo "*** Mount ${hd}: KO!"
        return 1
        
    fi
}

is_online() {
    if [ "$(bioctl "$1" | awk '/softraid0/ { printf "%s",$3 }')" = 'Online' ]; then
        return 0
    else
        return 1
    fi
}

mount_crypt_device() {
    
    for hd in sd0 sd1; do
        get_duid
    
        if [ -n "${duid}" ] ; then
            get_passwd
            get_tmp
            get_slide

            if [ -n "${slide}" ]; then attach_disk; fi
            
        fi

    done
    
    if is_online sd3 && is_online sd4; then
        
        hd="sd5"
        get_duid
        
        if [ -n "${duid}" ]; then
            get_slide
            build_dirs
        
            if ! _mount; then
                error=1
                if check_fs; then
                    if _mount; then error=0; fi
                
                fi
                
            fi
            
        fi
        
        if [ ${error} -eq 0 ]; then sync; fi
        
    fi

}

set_symlink() {

    ln -s "${dir_duid}" "${symlink}"
    
}

_shutdown() {
    
    hd="sd5"
    logger "$0: umount:${dir_mnt}"
    if umount -f "${dir_mnt}"; then
        echo "*** Unmount ${hd}: OK!"
        sleep 3
        sync
        
        get_duid
        dir_duid="/mnt/${duid}"
        delete_all
    fi
    
    for hd in sd0 sd1; do
        get_duid
        #build_dirs
        
        if [ -n "${duid}" ]; then
            get_tmp
            get_device
            
            logger "$0: bioctl -d ${device}"
            if bioctl -d "${device}"; then
                echo "*** bioctl detaches ${device}: OK!"
                rm -fP "${file_tmp}"
        
            else
                logger "$0: bioctl can't detache ${device}!"
                echo "*** bioctl detaches ${device}: OK!"
            
            fi
        
        fi
        
    done
    
}

################################################################################
###
##
#   EXECUTION
##
###
################################################################################

case "$menu" in
    "mount") mount_crypt_device ;;
    "umount") _shutdown ;;
    *) help; exit ;;
esac
